# ck_area 网格数据处理原型

## 基于PIXI的网格数据处理原型,可用于排号,排位等功能开发

## 网格功能演示

[https://clake.gitee.io/ck_area](https://clake.gitee.io/ck_area)

## 选择座位功能演示
[https://clake.gitee.io/ck_area/select.html](https://clake.gitee.io/ck_area/select.html)