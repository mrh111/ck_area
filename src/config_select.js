/**
 * Created by CLAKE on 2016/10/26.
 */
export default {
    rows:20, //行数
    cols:40, //列数
    rectWidth:20, //单个格子宽度
    rectHeight:20, //单个格子高度
    element:'area_select', //默认生成区域的DOM节点名称
    fps:true //是否显示FPS
};