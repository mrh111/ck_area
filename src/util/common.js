/**
 * Created by clakeboy on 16/9/15.
 */
export function Calculate(count,unit) {
    return count % unit == 0 ? parseInt(count / unit) : parseInt(count / unit) + 1;
}

export function GetElementXY(e) {
    var parent = arguments[1]?arguments[1]:undefined;
    var t = {};
    t['top'] = e.offsetTop;
    t['left'] = e.offsetLeft;
    var scrollTop = 0;
    var scrollLeft = 0;
    var topScroll = 0;
    var leftScroll = 0;
    while ((e = e.offsetParent) && e != parent) {
        t['top'] += e.offsetTop;
        t['left'] += e.offsetLeft;
        topScroll = e.scrollTop;
        scrollTop += topScroll;
        leftScroll = e.scrollLeft;
        scrollLeft += leftScroll;
    }
    t['top'] = t['top'] - (scrollTop - topScroll);
    t['left'] = t['left'] - (scrollLeft - leftScroll);

    scrollTop = topScroll = scrollLeft = leftScroll = parent = null;
    return t;
}

export function CloneObject(obj) {
    var txt=JSON.stringify(obj);
    return JSON.parse(txt);
}

export function String2Hex(str_hex) {
    return parseInt('0x'+str_hex.substr(1),16);
}

export function Extend(old_obj,new_obj) {
    if (typeof new_obj != 'object') {
        return old_obj;
    }
    for (var k in new_obj) {
        old_obj[k] = new_obj[k];
    }
    return old_obj;
}

export var Storage = {
    set:function(key,value){
        if (window.localStorage) {
            localStorage.setItem(key,JSON.stringify(value));
        }
    },
    get:function(key){
        if (window.localStorage) {
            return JSON.parse(localStorage.getItem(key));
        } else {
            return null;
        }
    },
    clear:function(){
        if (window.localStorage) {
            localStorage.clear();
        }
    },
    remove:function(key){
        if (window.localStorage) {
            localStorage.removeItem(key);
        }
    }
};

