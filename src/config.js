/**
 * Created by clakeboy on 16/9/13.
 */
export default {
    lineLength:20, //标尺宽度
    rows:20, //行数
    cols:40, //列数
    rectWidth:30, //单个格子宽度
    rectHeight:30, //单个格子高度
    element:'area_select', //默认生成区域的DOM节点名称
    pointType:'def',//区块是默认还是正反序 值有: def|asc|desc
    fps:true, //是否显示FPS
    staff:{ //标尺线设置
        order:{
            hor:'asc',
            ver:'desc'
        }, //string | json, string 值为 asc | desc , json值为 {hor:'asc',ver:'desc'}
        type:'abc', //num | abc 只支持纵向
        
    },
    staffEmpty:[
        // {type:'hor',value:10} 横向标尺第11列留空
        // {type:'ver',value:10} 纵向标尺第11行留空
    ],
    blockTip:{
        template:'区域位置: {block}\n' +
                '所在行: {row}\n' +
                '所在列: {col}\n' +
                '颜色: {color}\n' +
                '是否保留: {is_retain}\n' +
                '是否打印: {is_point}'
        // format方法可选的,设置之后以format方法返回的提示文字显示
        // ,format:(template,item)=>{
        //     return '';
        // }
    },
    /**
     * 底部提示文字模板,变量:
     * grid: 已设置的有数据的区块数量
     * pointer: 鼠标在区域上的坐标
     * selected: 已经选中的区块数量
     */
    bottomTip:'G: {grid} | {pointer} | S: {selected}'
};