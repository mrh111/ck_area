/**
 * Created by CLAKE on 2016/8/9.
 */
var ip = require('ip');
const ip_address = ip.address();
var webpackConfig = require('./webpack.config');
var webpack = require('webpack');
webpackConfig.entry = {
    area: [
        //资源服务器地址
        'webpack/hot/dev-server',
        'webpack-hot-middleware/client?reload=true',
        './src/app.js'
    ],
    area_select: [
        //资源服务器地址
        'webpack/hot/dev-server',
        'webpack-hot-middleware/client?reload=true',
        './src/select_app.js'
    ]
};

webpackConfig.plugins = [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new webpack.DefinePlugin({
        'process.env.NODE_ENV': '"development"'
    })
];

// webpackConfig.plugins.push(new webpack.HotModuleReplacementPlugin());
// webpackConfig.plugins.push(
//     new webpack.DefinePlugin({
//         'process.env.NODE_ENV': '"development"'
//     })
// );

//webpackConfig.devServer = {
//    historyApiFallback: true,
//    hot: true,
//    inline: true,
//    progress: true,
//    colors:true
//};

webpackConfig.devtool = 'eval-source-map';

export default webpackConfig;