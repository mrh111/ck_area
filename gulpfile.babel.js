/**
 * Created by CLAKE on 2016/8/7.
 */
import gulp from 'gulp';
import browserSync from 'browser-sync';
import webpack from 'webpack';
import webpackDevMiddleware from 'webpack-dev-middleware';
import webpackHotMiddleware from 'webpack-hot-middleware';
import webpackConfigDev from './webpack.dev.config';

gulp.task('server', () => {
    const bundler = webpack(webpackConfigDev);
    const bs = browserSync.create();

    bs.init({
        logPrefix: 'AMT',
        server: {
            baseDir: ['dist'],
            browser: ["google chrome", "firefox"],
            middleware: [
                webpackDevMiddleware(bundler, {
                    publicPath: '/', // webpackConfigDev.output.publicPath,
                    stats: {colors: true},
                    lazy: false,
                    watchOptions: {
                        aggregateTimeout: 300,
                        poll: true
                    }
                }),
                webpackHotMiddleware(bundler)
            ]
        }
    });
});

gulp.task('demo',()=>{
    const bs = browserSync.create();
    bs.init({
        logPrefix: 'AMT',
        server: {
            baseDir: ['dist']
        },
        port:8080,
        browser: ["google chrome", "firefox"]
    });
});

gulp.task('default', ['server']);