/**
 * Created by CLAKE on 2016/8/9.
 */
var ip = require('ip');
var webpack = require('webpack');
var path = require('path');
var commonsPlugin = new webpack.optimize.CommonsChunkPlugin('common.js');
var processPlugin = new webpack.DefinePlugin({
    "process.env": {
        NODE_ENV: JSON.stringify("production")
    }
});

const ip_address = ip.address();

module.exports = {
    //插件项
    plugins: [
        processPlugin
    ],
    //页面入口文件配置
    entry: {
        //主文件
        area : './src/app.js',
        area_select: './src/select_app.js'
    },
    //入口文件输出配置
    // output: {
    //     path: 'public/dist/js/page',
    //     filename: '[name].js'
    // },
    output: {
        path: `${__dirname}/dist`,
        filename: '[name].js'
    },
    module: {
        //加载器配置
        loaders: [
            {
                test: /\.less$/,
                loader: 'style!css!less'
            },
            {
                test: /\.css$/,
                loader: 'style-loader!css-loader'
            },
            { test: /\.woff$/, loader: "url-loader?limit=10000&mimetype=application/font-woff" },
            { test: /\.ttf$/,  loader: "url-loader?limit=10000&mimetype=application/octet-stream" },
            { test: /\.eot$/,  loader: "file-loader" },
            { test: /\.svg$/,  loader: "url-loader?limit=10000&mimetype=image/svg+xml" },
            {
                test: /\.js$/,
                loader: 'babel',
                exclude: /node_modules/
            },
            {
                test: /\.(jpe?g|png|gif)$/i,
                loader: 'url?limit=10000&name=img/[hash:8].[name].[ext]'
            }
        ]
    },
    //其它解决方案配置
    resolve: {
        extensions: ['', '.js', '.json', '.less', '.jsx']
        // ,alias: {
        //     AppStore : 'js/stores/AppStores.js',
        //     ActionType : 'js/actions/ActionType.js',
        //     AppAction : 'js/actions/AppAction.js'
        // }
    },
    node: {
        fs: 'empty'
    }
};
